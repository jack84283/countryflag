/**
 * 
 */
package com.jack.countryflag.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.jack.countryflag.model.db.BaseEntity;

/**
 * @author jack
 *
 */
public interface IBaseDao<E extends BaseEntity, K> {

	/**
	 * Save entity
	 * @param  E	an entity extended from BaseEntity     
	 */
	public void persist(E entity);
	
	/**
	 * Delete entity
	 * @param  E	an entity extended from BaseEntity     
	 */
	public void remove(E entity);
	
	/**
	 * Query all entities using named query (findAll)
	 * @return	a list of all entities 	           
	 */
	public List<E> listAll();
	
	/**
	 * Search 	entity by Id
	 * @param  	K	Id of entity
	 * @return 	E	an entity with Id as input if found	    
	 * @see        
	 */
	public E findById(K id);
	
	/**
	 * Search 	entity using named query defined with indexed parameters;
	 * @param 	name 	name of the named query
	 * @param 	indexedParameters	parameters of the named query using index, e.g. 1?, 2? etc.
	 * @return 	E
	 */
	public List<E> findByNamedQuery(String name, List<String> indexedParameters);
	
	/**
	 * 
	 * @return EntityManagerFactory
	 */
	public EntityManagerFactory getEntityManagerFactory();
	
	/**
	 * 
	 * @return Class<E>
	 */
	public Class<E> getEntityClass();
	
	/**
	 * 
	 * @return EntityManager
	 */
	public EntityManager getEntityManager();
	
	/**
	 * Start Transaction with current entity manager;
	 */
	public void beginTransaction();
	
	/**
	 * Commit the transaction of current entity manager;
	 */
	public void commitTranscation();
	
	/**
	 * Roll back the transaction of current entity manager;
	 */
	public void rollbackTransaction();
	
}
