/**
 * 
 */
package com.jack.countryflag.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.jack.countryflag.model.db.BaseEntity;

/**
 * @author jack
 *
 */
public abstract class JpaDao<E extends BaseEntity, K> implements IBaseDao<E, K> {

	protected EntityManagerFactory entityManagerFactory;
	protected EntityManager entityManager;
	protected Class<E> entityClass;
	protected String entityName;
	
	private EntityTransaction entityTransaction;
	
	@SuppressWarnings("unchecked")
	public JpaDao(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.entityManager = this.entityManagerFactory.createEntityManager();
		ParameterizedType genericSuprclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuprclass.getActualTypeArguments()[0];
		
		String[] splitted = this.entityClass.toString().split("\\.", -1);
		this.entityName = splitted[splitted.length-1];
	}
	
	public JpaDao(EntityManager entityManager) {
		this(entityManager.getEntityManagerFactory());
		this.entityManager = entityManager;
	}
	
	@Override
	public void persist(E entity) {
		this.entityManager.persist(entity);
	}
	
	@Override
	public void remove(E entity) {
		this.entityManager.remove(entity);
	}
	
	@Override
	public E findById(K id) {
		return this.entityManager.find(entityClass, id);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<E> listAll() {
		
		Query query = this.entityManager.createQuery(this.entityName + ".findAll");
		if(query != null) {
			return (List<E>) query.getResultList();
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @return Class<E>
	 */
	@Override
	public Class<E> getEntityClass() {
		return entityClass;
	}

	/**
	 * 
	 * @return EntityManagerFactory
	 */
	@Override
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	/**
	 * 
	 * @return EntityManager
	 */
	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * 
	 * @return String
	 */
	public String getEntityName() {
		return entityName;
	}
	
	@Override
	public void beginTransaction() {
		// TODO Auto-generated method stub
		if(this.entityTransaction == null || this.entityTransaction.isActive() == false) {
			this.entityTransaction = this.entityManager.getTransaction();
		}
		
		this.entityTransaction.begin();
	}


	@Override
	public void commitTranscation() {
		// TODO Auto-generated method stub
		if(this.entityTransaction != null && this.entityTransaction.isActive()) {
			this.entityTransaction.commit();
		}
	}
	
	@Override
	public void rollbackTransaction() {
		// TODO Auto-generated method stub
		if(this.entityTransaction != null && this.entityTransaction.isActive()) {
			this.entityTransaction.rollback();
		}
	}
}
