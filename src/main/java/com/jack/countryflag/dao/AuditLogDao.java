/**
 * 
 */
package com.jack.countryflag.dao;

import java.util.List;

import com.jack.countryflag.model.db.Auditlog;

/**
 * @author jack
 *
 */
public interface AuditLogDao extends IBaseDao<Auditlog, String> {

	List<Auditlog> readAuditLogByProcessId(String processId);
	List<Auditlog> readAuditLogByObject(String object);
	
}
