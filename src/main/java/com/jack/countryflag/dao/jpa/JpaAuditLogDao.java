/**
 * 
 */
package com.jack.countryflag.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import com.jack.countryflag.dao.AuditLogDao;
import com.jack.countryflag.dao.JpaDao;
import com.jack.countryflag.model.db.Auditlog;


/**
 * @author jack
 *
 */

@Component
public class JpaAuditLogDao extends JpaDao<Auditlog, String> implements AuditLogDao {

	public JpaAuditLogDao(EntityManager entityManager) {
		super(entityManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Auditlog> readAuditLogByProcessId(String processId) {
		// TODO Auto-generated method stub
		TypedQuery<Auditlog> query = this.entityManager.createQuery("SELECT a FROM Auditlog a WHERE a.processId = ?1", Auditlog.class);
		query.setParameter(1,  processId);
		
		return query.getResultList();
	
	}

	@Override
	public List<Auditlog> readAuditLogByObject(String object) {
		// TODO Auto-generated method stub
		TypedQuery<Auditlog> query = this.entityManager.createQuery("SELECT a FROM Auditlog a WHERE a.object = ?1", Auditlog.class);
		query.setParameter(1,  object);
		
		return query.getResultList();
	}

	@Override
	public List<Auditlog> findByNamedQuery(String name, List<String> indexedParameters) {
		// TODO Auto-generated method stub
		return null;
	}

}
