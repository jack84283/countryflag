/**
 * 
 */
package com.jack.countryflag.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jack.countryflag.dao.AuditLogDao;
import com.jack.countryflag.model.db.Auditlog;

/**
 * @author jack
 *
 */

@Component
public class AuditlogService {

	@Autowired
	private AuditLogDao auditLogDao;
	
	public void writeAuditLogContinent(String Continent) {
		
	}
	
	public void writeAuditLogCountries(String[] Counties) {
		
	}
	
	public List<Auditlog> readAuditLogByProcessId(String processId) {
		
		List<Auditlog> logs = this.auditLogDao.readAuditLogByProcessId(processId);
		return logs;
		
	}
	
	public List<Auditlog> readAuditLogByObject(String object) {
		
		List<Auditlog> logs = this.auditLogDao.readAuditLogByObject(object);
		return logs;
	}
	
}
