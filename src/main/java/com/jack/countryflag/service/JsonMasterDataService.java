package com.jack.countryflag.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jack.countryflag.model.Masterdata;

@Service
public class JsonMasterDataService {

	@Value("${json.countryflag.filename}")
	private String json;
	
	private List<Masterdata> masterData = new ArrayList<>();
	
	@PostConstruct
	public void init() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = new ClassPathResource(json).getInputStream();
		
		masterData = mapper.readValue(is, new TypeReference<ArrayList<Masterdata>>(){});
	}
	
	public List<Masterdata> getMasterdata() {
		
		return this.masterData;
	}
	
}
