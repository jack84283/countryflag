/**
 * 
 */
package com.jack.countryflag.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jack.countryflag.dao.AuditLogDao;
import com.jack.countryflag.model.Country;
import com.jack.countryflag.model.Masterdata;
import com.jack.countryflag.model.db.Auditlog;

/**
 * @author jack
 *
 */

@Component
public class SearchService {
	
	/*@Autowired
	private AuditLogDao auditLogDao;*/
	
	@Autowired
	private JsonMasterDataService jsonMasterDataService;

	public List<Country> searchByContient(String continent) {
		List<Masterdata> masterdata = this.readJSON();
		List<Country> ret = null;
		
		if(masterdata != null) {
			for(Object c: masterdata) {
				Masterdata d = (Masterdata)c;
				if(d.getContinent().equalsIgnoreCase(continent)) {
					ret = d.getCountries();
					
					break;
				}
			}
		}
		
		//this.writeAuditLogByContient(continent);
		return ret;
	}
	
	public List<Country> searchByCountries(String countries) {
		List<Masterdata> masterdata = this.readJSON();
		List<Country> ret = null;
		
		if(countries == null || "".equals(countries.trim())) return ret;
		
		String[] arryCountry = countries.split(",");
		
		if(masterdata != null && arryCountry != null) {
			ret = new ArrayList<Country>();
			for(String country: arryCountry) {
				for(Masterdata d : masterdata) {
					List<Country> lstCountry = d.getCountries();
					for(Country c : lstCountry) {
						if(c.getName().equalsIgnoreCase(country)) {
							ret.add(c);
							break;
						}
					}
				}
				if(arryCountry.length == ret.size()) {
					break;
				}
			}

		}
		
		//this.writeAuditLogByCounties(arryCountry);
		return ret;
	}
	
	private List<Masterdata> readJSON() {
		return this.jsonMasterDataService.getMasterdata();
	}
	
	/*private void writeAuditLogByContient(String contnient) {
		
		Auditlog log = new Auditlog();
		log.setId(UUID.randomUUID().toString());
		log.setAuditAction("Read");
		log.setAuditType("CountryFlag");
		log.setProcessId(UUID.randomUUID().toString()); // should be passed in;
		log.setObject(contnient);
		log.setUpdatedBy("");
		log.setUpdatedOn(Calendar.getInstance().getTime());
		
		this.auditLogDao.beginTransaction();
		this.auditLogDao.persist(log);
		this.auditLogDao.commitTranscation();
		
	}
	
	private void writeAuditLogByCounties(String[] countries) {
		
		if(countries != null && countries.length > 0) {
			
			this.auditLogDao.beginTransaction();
			String processId = UUID.randomUUID().toString();
			
			for(String country : countries) {
				Auditlog log = new Auditlog();
				log.setId(UUID.randomUUID().toString());
				log.setAuditAction("Read");
				log.setAuditType("CountryFlag");
				log.setProcessId(processId);
				log.setObject(country);
				log.setUpdatedBy("");
				log.setUpdatedOn(Calendar.getInstance().getTime());
				this.auditLogDao.persist(log);
			}
			
			this.auditLogDao.commitTranscation();
			
		}
		
	}*/
}
