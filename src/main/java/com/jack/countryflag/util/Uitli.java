package com.jack.countryflag.util;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Uitli {

	public static ArrayList<Object> ConvertJson2ArrayEntities(String json) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return mapper.readValue(json, new TypeReference<ArrayList<Object>>(){});
	}
}
