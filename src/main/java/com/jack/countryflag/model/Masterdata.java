/**
 * 
 */
package com.jack.countryflag.model;

import java.util.List;

/**
 * @author jack
 *
 */
public class Masterdata {

	private String continent;
	private List<Country> countries;
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public List<Country> getCountries() {
		return countries;
	}
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	
}
