package com.jack.countryflag.model.db;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the flag database table.
 * 
 */
@Entity
@Table(name="flag", schema="countryflag")
@NamedQuery(name="Flag.findAll", query="SELECT f FROM Flag f")
public class Flag extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String continet;

	private String country;

	private String flag;

	public Flag() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContinet() {
		return this.continet;
	}

	public void setContinet(String continet) {
		this.continet = continet;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}