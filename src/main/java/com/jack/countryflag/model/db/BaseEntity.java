/**
 * 
 */
package com.jack.countryflag.model.db;

import java.io.Serializable;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author jack
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PostLoad
	protected void postLoad() {

	}

	@PrePersist
	protected void prePersiste() {

	}

	@PostPersist
	protected void postPersist() {

	}

	@PreUpdate
	protected void preUpdate() {

	}

	@PostUpdate
	protected void postUpdate() {

	}

	@PreRemove
	protected void preRemove() {

	}

	@PostRemove
	protected void postRemove() {

	}
}
