/**
 * 
 */
package com.jack.countryflag.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.jack.countryflag.model.Country;
import com.jack.countryflag.service.SearchService;

import io.swagger.annotations.ApiOperation;

/**
 * @author jack
 *
 */

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/")
public class SearchController {
	
	@Autowired
	private SearchService serachService;

	@RequestMapping(method=RequestMethod.GET, value="/search/Continent/{name}", produces = {"application/json"})
	@ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Search country flags by continent, list of countries", notes = "")
	@ResponseBody
    public List<Country> searchContient(@PathVariable("name") String name) {
    	
		return serachService.searchByContient(name);
    }
	
	@RequestMapping(method=RequestMethod.GET, value="/search/Countries/{list}", produces = {"application/json"})
	@ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Search country flags by continent, list of countries", notes = "")
	@ResponseBody
    public List<Country> searchCountries(@PathVariable("list") String list) {
    	
		return serachService.searchByCountries(list);
    }
}
