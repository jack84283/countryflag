/**
 * 
 */
package com.jack.contryflag.exception;

/**
 * @author jack
 *
 * For HTTP 404 errors
 */
public final class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 967500106990010911L;

	public ResourceNotFoundException() {
        super("Resurce not found!");
    }

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException(Throwable cause) {
        super(cause);
    }

}