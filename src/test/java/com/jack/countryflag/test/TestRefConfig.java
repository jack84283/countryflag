/**
 * 
 */
package com.jack.countryflag.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.annotation.Bean;

import com.jack.countryflag.dao.AuditLogDao;
import com.jack.countryflag.dao.jpa.JpaAuditLogDao;
import com.jack.countryflag.service.AuditlogService;
import com.jack.countryflag.service.JsonMasterDataService;
import com.jack.countryflag.service.SearchService;

/**
 * @author jack
 *
 */
public class TestRefConfig {
	
	private static final String PERSISTENCE_UNIT_NAME = "country-flag-test";
	
	@Bean
	public EntityManagerFactory entityManagerFactory() {
		return Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}

	@Bean
	public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
		return entityManagerFactory.createEntityManager();
	}
	
	@Bean
	public AuditLogDao auditLogDao(EntityManager entityManager) {
		return new JpaAuditLogDao(entityManager);
	}
	
	@Bean
	public JsonMasterDataService jsonMasterDataService() {
		return new JsonMasterDataService();
	}
	
	@Bean
	public AuditlogService auditlogService() {
		return new AuditlogService();
	}
	
	@Bean
	public SearchService searchService() {
		return new SearchService();
	}

}
