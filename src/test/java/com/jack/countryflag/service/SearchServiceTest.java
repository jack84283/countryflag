/**
 * 
 */
package com.jack.countryflag.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jack.countryflag.model.Country;

/**
 * @author jack
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SearchService.class, JsonMasterDataService.class})
@EnableConfigurationProperties
public class SearchServiceTest {

	@Autowired
	private SearchService searchService;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		//AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestRefConfig.class);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() {
		
	}

	@Test
	public void testSearchByContient() {
		
		List<Country> country = this.searchService.searchByContient("Asia");
		assertEquals(country.size(), 5);
	}
	
	@Test
	public void testSearchByContient_Uppercase() {
		
		List<Country> country = this.searchService.searchByContient("ASIA");
		assertEquals(country.size(), 5);
	}
	
	@Test
	public void testSearchByContient_Lowercase() {
		
		List<Country> country = this.searchService.searchByContient("asia");
		assertEquals(country.size(), 5);
	}
	
	@Test
	public void testSearchByContient_Mixedcase() {
		
		List<Country> country = this.searchService.searchByContient("AsiA");
		assertEquals(country.size(), 5);
	}
	
	@Test
	public void testSearchByContient_NullInput() {
		
		List<Country> country = this.searchService.searchByContient(null);
		assertNull(country);
	}
	
	@Test
	public void testSearchByContient_NotExist() {
		
		List<Country> country = this.searchService.searchByContient("AAsia");
		assertNull(country);
	}
	
	@Test
	public void testSearchByCountries() {
		
		List<Country> country = this.searchService.searchByCountries("China,India");
		assertEquals(country.size(), 2);
	}
	
	@Test
	public void testSearchByCountries_DifferContient() {
		
		List<Country> country = this.searchService.searchByCountries("China,usa,South Africa");
		assertEquals(country.size(), 3);
	}
	
	@Test
	public void testSearchByCountries_NullInuput() {
		
		List<Country> country = this.searchService.searchByCountries(null);
		assertNull(country);
	}
	
	@Test
	public void testSearchByCountries_Empty() {
		
		List<Country> country = this.searchService.searchByCountries(" ");
		assertNull(country);
	}
	
	@Test
	public void testSearchByCountries_NotExist_Mixedcase() {
		
		List<Country> country = this.searchService.searchByCountries("South.Africa,gerMany");
		assertEquals(country.size(), 1);
	}

}
